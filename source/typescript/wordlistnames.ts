var boxes = document.getElementsByClassName("wordlistname");
var scores = document.getElementsByClassName("score");
var wordlists = [];

var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    let a: Array<Map<Text,any>> = JSON.parse(this.responseText);
    let b: Boolean = (scores.length != 0);
    for (let i:number = 0; i<boxes.length; i++){
      if (b) {
        scores[i].innerText += "/"+a[Number(boxes[i].innerText)]["questions"].length.toString()
      };
      boxes[i].innerText = a[Number(boxes[i].innerText)]["name"]; // scores declared in scores.html
    }
  } else {
    console.log("Getting wordlist names failed.");
  }
}
xmlhttp.open("GET", static_root + "/plain/wordlists.json", true); // static_root is declared in the header of base.html
xmlhttp.send();