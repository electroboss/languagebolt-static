var navbar_shown = false;

function showNavbar(){
  navbar_shown = !navbar_shown;
  var navbar_elements:HTMLCollectionOf<Element> = document.getElementsByClassName("navbar-mobile-element");
  for (var i=0; i<navbar_elements.length; i++) {
    navbar_elements[i].style.display = navbar_shown ? "inherit" : "none";
  }
}
