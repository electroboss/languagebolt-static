var wordlist_select = document.getElementById("wordlist");
var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    let wordlists = JSON.parse(this.responseText)
    for (var i:number=0;i<wordlists.length; i++) {
      wordlist_select.innerHTML += "<option value=\""+i.toString()+"\">"+wordlists[i]["name"]+"</option>";
    }
  }
}
xmlhttp.open("GET", static_root + "/plain/wordlists.json", true); // static_root is declared in the header file
xmlhttp.send();
