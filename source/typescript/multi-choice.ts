var answerlist:Array<string> = [];
var questionlist:Array<string> = [];
var questionsleft:Array<number> = [];
var score:number = 0;
var words:Array<number> = [];
var correct:number = 0;
var correctoffour:number = 0;
var test;

// get wordlists at static/wordlists.json
var xmlhttp = new XMLHttpRequest();
xmlhttp.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    let params = new URLSearchParams(location.search);
    let thing = JSON.parse(this.responseText)[Number(params.get("wordlist"))];
    answerlist = thing["answers"];
    questionlist = thing["questions"];
  }
}
xmlhttp.open("GET", static_root + "/plain/wordlists.json", true); // static_root is declared in the header file
xmlhttp.send();
// done

function go() : void {
  document.getElementById("optiontable").style.display = "inherit";
  document.getElementById("start").style.display = "none";
  for (var i:number = 0; i<questionlist.length; i++) {
    questionsleft.push(i);
  }
  update();
}
function update() : void {
  var templist:Array<number> = Array();
  for (var i:number = 0; i<answerlist.length; i++) {
    templist.push(i)
  } // templist = list(range(len(answerlist)))

  correct = questionsleft[Math.floor(questionsleft.length * Math.random())];

  words = [];
  templist.splice(correct,1);
  for (var i:number = 0; i<3; i++) {
    let templistindex = Math.floor(templist.length * Math.random());
    words.push(templist[templistindex]);
    templist.splice(templistindex,1);
  }
  
  correctoffour = 4 * Math.random();
  words.splice(correctoffour, 0, correct);

  for (var i:number = 0; i<4; i++) {
    document.getElementById("option-"+i.toString()).innerText = answerlist[words[i]];
  }
  document.getElementById("question").innerText = questionlist[correct];
}
function update_score() : void {
  document.getElementById("score").innerText = "Score: "+score.toString();
}
function option(id:number) {
  if (words[id] == correct) {
    score += 1;
    update_score();
  }
  questionsleft.splice(questionsleft.indexOf(correct),1);
  if (questionsleft.length == 0){
    let params = new URLSearchParams(location.search);
    let xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "game");
    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    let tries = 10;
    xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        window.location.replace(window.location.href + "&done=1");
      } else {
        console.log(this.responseText,this.status);
        if (tries-- >= 0) {
          let xmlhttp = new XMLHttpRequest();
          xmlhttp.open("POST", "game");
          xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
          xmlhttp.send("score="+score.toString()+"&gametype="+encodeURI(params.get("gametype"))+"&game="+encodeURI(params.get("game"))+"&wordlist="+encodeURI(params.get("wordlist"))+"&random="+encodeURI(Math.random().toString()));
        } else {
          console.log("Didn't work");
          document.getElementById("score").innerText = "Score send didn't work\n<br /><a href='/'>Homepage</a>";
        }
      }
    };
    xmlhttp.send("random="+encodeURI(Math.random().toString())+"&score="+score.toString()+"&gametype="+encodeURI(params.get("gametype"))+"&game="+encodeURI(params.get("game"))+"&wordlist="+encodeURI(params.get("wordlist")));
    document.getElementById("score").innerText = "Redirecting";
  }
  update();
}
