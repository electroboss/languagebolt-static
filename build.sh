rm -rf public
mkdir public
mkdir public/css
cd source/sass
for x in $(find -type f); do sass "$x" "../../public/css/${x%.scss}.css"; done
cd ../..
mkdir public/js
cd source/typescript
for x in $(find -type f); do tsc "$x" --out "../../public/js/${x%.ts}.js"; done
cd ../..
mkdir public/plain
cp source/plain/* public/plain/
